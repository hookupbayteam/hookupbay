//
//  AnimationTabbarViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 31.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "AnimationTabbarViewController.h"
#import "Helper.h"

@interface AnimationTabbarViewController () 

@end

@implementation AnimationTabbarViewController


-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if (self.isPresenting)
        [self executePresentationAnimation:transitionContext];
    else
        [self executeDismissalAnimation:transitionContext];
}


-(void)executePresentationAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIView* inView = [transitionContext containerView];
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [inView addSubview:toViewController.view];
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        if (!self.reverse)
            toViewController.view.frame = CGRectMake(SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
        else
            toViewController.view.frame = CGRectMake(-SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
    } else {
        if (!self.reverse)
            toViewController.view.frame = CGRectMake(SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
        else
            toViewController.view.frame = CGRectMake(-SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
    }
    
    [UIView animateWithDuration:self.duration
                     animations:^{
                         if (UIDeviceOrientationIsLandscape(orientation)) {
                            fromViewController.view.frame = CGRectMake(SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
                            toViewController.view.frame = CGRectMake(0, 0, SelfViewHeight, SelfViewWidth);
                         } else {
                            fromViewController.view.frame = CGRectMake(SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
                            toViewController.view.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
                         }
                     } completion:^(BOOL finished) {
                          [transitionContext completeTransition:YES];
                     }];
    
}

-(void)executeDismissalAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIView* inView = [transitionContext containerView];
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [inView insertSubview:toViewController.view belowSubview:fromViewController.view];
    
    CGPoint centerOffScreen = inView.center;
    centerOffScreen.y = (-1)*inView.frame.size.height;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        if (!self.reverse)
            toViewController.view.frame = CGRectMake(SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
        else
            toViewController.view.frame = CGRectMake(-SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
    } else {
        if (!self.reverse)
            toViewController.view.frame = CGRectMake(SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
        else
            toViewController.view.frame = CGRectMake(-SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
    }
    
    [UIView animateWithDuration:self.duration
                     animations:^{
                         if (UIDeviceOrientationIsLandscape(orientation)) {
                             if (!self.reverse) {
                                 fromViewController.view.frame = CGRectMake(-SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
                                 toViewController.view.frame = CGRectMake(0, 0, SelfViewHeight, SelfViewWidth);
                             } else {
                                 fromViewController.view.frame = CGRectMake(SelfViewHeight, 0, SelfViewHeight, SelfViewWidth);
                                 toViewController.view.frame = CGRectMake(0, 0, SelfViewHeight, SelfViewWidth);
                             }
                         } else {
                             if (!self.reverse) {
                                 fromViewController.view.frame = CGRectMake(-SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
                                 toViewController.view.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
                             } else {
                                fromViewController.view.frame = CGRectMake(SelfViewWidth, 0, SelfViewWidth, SelfViewHeight);
                                toViewController.view.frame = CGRectMake(0, 0, SelfViewWidth, SelfViewHeight);
                             }
                         }
                     } completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }];
}


- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return self.duration;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    _isPresenting = YES;
    
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    _isPresenting = NO;
    return self;
}

@end
