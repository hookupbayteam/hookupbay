//
//  AccountView.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 01.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "AccountView.h"
#import "UIView+Border.h"
#import "Helper.h"

@interface AccountView ()
{
    NSInteger photoIndex;
    NSInteger activeView; //1 - first, 2 - second
}

@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextView *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstPhotoCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondPhotoCountLabel;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *firstPhotoViewLeft;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondPhotoViewLeft;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *firstPhotoWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;

@end

@implementation AccountView

-(NSMutableArray *)photos
{
    if (!_photos) _photos = [[NSMutableArray alloc] init];
    _photos = self.account[@"photos"];
    return _photos;
}


-(void)updateUI
{
    photoIndex = 0;
    
    if (self.photos.count > 0) {
        self.photo.image = (UIImage *)[self.account[@"photos"] objectAtIndex:photoIndex];
    }
    
    activeView = 1;
    self.firstPhotoViewLeft.constant = 0.0;
    self.secondPhotoViewLeft.constant = 0.0;
    [self layoutIfNeeded];
    
    self.firstPhotoCountLabel.text = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)self.photos.count];
    [self updatePhotoCountLabels];
    
    NSString *text = [NSString stringWithFormat:@"%@, %@", self.account[@"name"], self.account[@"age"]];
    UIColor *ageColor = UIColorFromRGB(0x8e8e8e);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           ageColor, NSForegroundColorAttributeName, nil];
    NSRange range = [text rangeOfString:@","];
    if (!(range.location == NSNotFound)) {
        NSString *ageStr = [text substringFromIndex:range.location];
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
        [attributedText setAttributes:attrs range:[text rangeOfString:ageStr]];
    }
    self.nameLabel.text = text;
    [self.nameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:17]];
    
    self.questionLabel.text = self.account[@"question"];
    self.questionLabel.contentInset = UIEdgeInsetsMake(-5,-5,0,0);
    [self.questionLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:self.account[@"question"]
                                                                         attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:15.0f]}];
    
    CGFloat height = [attributedText boundingRectWithSize:(CGSize){280.0f, CGFLOAT_MAX}
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                  context:nil].size.height;
   
    self.textViewHeight.constant = height*1.5;
    [self layoutIfNeeded];

    self.swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showPreviousPhoto)];
    [self.swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:self.swipeRight];

    self.swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showNextPhoto)];
    [self.swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:self.swipeLeft];
}

-(void)updatePhotoCountLabels
{
    if (activeView == 2)
        self.firstPhotoCountLabel.text = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)self.photos.count];
    if (activeView == 1)
        self.secondPhotoCountLabel.text = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)self.photos.count];
}

-(void)showNextPhoto
{
    if (photoIndex < self.photos.count-1) {
        
        //prepare second view
        if (activeView == 1) {
            self.secondPhotoViewLeft.constant = 320.0;
            self.secondPhotoView.alpha = 1.0;
            [self layoutIfNeeded];
        } else {
            self.firstPhotoViewLeft.constant = 320.0;
            self.firstPhotoView.alpha = 1.0;
            [self layoutIfNeeded];
            
        }

        photoIndex++;
        
        //create image view with next photo
        if (activeView == 1) {
            self.nextPhoto.image = (UIImage *)[self.photos objectAtIndex:photoIndex];
            self.firstPhotoViewLeft.constant = -320.0;
            self.secondPhotoViewLeft.constant = 0.0;
        }
        if (activeView == 2) {
            self.photo.image = (UIImage *)[self.photos objectAtIndex:photoIndex];
            self.secondPhotoViewLeft.constant = -320.0;
            self.firstPhotoViewLeft.constant = 0.0;
        }
        [self animateConstraintsForNextPhoto];
    }
}

-(void)showPreviousPhoto
{
    if (photoIndex > 0)  {
        
    //prepare second view
    if (activeView == 1) {
        self.secondPhotoViewLeft.constant = -320.0;
        self.secondPhotoView.alpha = 1.0;
        [self layoutIfNeeded];
    } else {
        self.firstPhotoViewLeft.constant = -320.0;
        self.firstPhotoView.alpha = 1.0;
        [self layoutIfNeeded];
    }
    
    photoIndex--;
    
    if (activeView == 1) {
         self.nextPhoto.image = (UIImage *)[self.photos objectAtIndex:photoIndex];
         self.firstPhotoViewLeft.constant = 320.0;
         self.secondPhotoViewLeft.constant = 0.0;
    }
    
    if (activeView == 2) {
         self.photo.image = (UIImage *)[self.photos objectAtIndex:photoIndex];
         self.secondPhotoViewLeft.constant = 320.0;
         self.firstPhotoViewLeft.constant = 0.0;
     }
    
    [self animateConstraintsForNextPhoto];
    }
}


- (void)animateConstraintsForNextPhoto
{
    [self updatePhotoCountLabels];
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         if (activeView == 1) {
                             activeView = 2;
                             self.firstPhotoView.alpha = 0.0;
                         } else {
                             activeView = 1;
                             self.secondPhotoView.alpha = 0.0;
                         }
                     }];
}

@end








