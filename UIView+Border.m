//
//  UIView+Border.m
//  TPW, draw borders for view
//
//  Created by Denis Morozov on 16.09.13.
//  Copyright (c) 2013 By EZD Productions, USA. All rights reserved.
//

#import "UIView+Border.h"

typedef enum {
	BorderViewTypeTop = 1,
	BorderViewTypeRight = 2,
	BorderViewTypeBottom = 3,
	BorderViewTypeLeft = 4,
}BorderViewType;

@interface BorderView : UIView

- (id)initWithType:(BorderViewType)type;
- (id)initWithType:(BorderViewType)type withColor:(UIColor *)color;
- (id)initWithType:(BorderViewType)type withColor:(UIColor *)color withWidth:(CGFloat)width;

@property (nonatomic, readonly) BorderViewType borderType;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

@end

@implementation UIView (Border)

#pragma mark - Public Methods

- (void)setBorder:(UIViewBorder)border
{
	[self setBorder:border withColor:[UIColor blackColor]];
}

- (void)setBorder:(UIViewBorder)border withColor:(UIColor *)color
{
	[self setBorder:border withColor:color withWidth:0.5f];
}

- (void)setBorder:(UIViewBorder)border withColor:(UIColor *)color withWidth:(CGFloat)width
{
	[self removeAllBorders];
	
	if (border & UIViewBorderTop)
	{
		BorderView *borderView = [[BorderView alloc] initWithType:BorderViewTypeTop withColor:color withWidth:width];
		[self addSubview:borderView];
	}
	
	if (border & UIViewBorderRight)
	{
		BorderView *borderView = [[BorderView alloc] initWithType:BorderViewTypeRight withColor:color withWidth:width];
		[self addSubview:borderView];
	}
	
	if (border & UIViewBorderBottom)
	{
		BorderView *borderView = [[BorderView alloc] initWithType:BorderViewTypeBottom withColor:color withWidth:width];
		[self addSubview:borderView];
	}
	
	if (border & UIViewBorderLeft)
	{
		BorderView *borderView = [[BorderView alloc] initWithType:BorderViewTypeLeft withColor:color withWidth:width];
		[self addSubview:borderView];
	}
}

- (void)setBorderColor:(UIColor *)color withBorder:(UIViewBorder)border
{
	if (border & UIViewBorderTop)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeTop];
		borderView.borderColor = color;
	}
	
	if (border & UIViewBorderRight)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeRight];
		borderView.borderColor = color;
	}
	
	if (border & UIViewBorderBottom)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeBottom];
		borderView.borderColor = color;
	}
	
	if (border & UIViewBorderLeft)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeLeft];
		borderView.borderColor = color;
	}
}

- (void)setBorderWidth:(CGFloat)width withBorder:(UIViewBorder)border
{
	if (border & UIViewBorderTop)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeTop];
		borderView.borderWidth = width;
	}
	
	if (border & UIViewBorderRight)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeRight];
		borderView.borderWidth = width;
	}
	
	if (border & UIViewBorderBottom)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeBottom];
		borderView.borderWidth = width;
	}
	
	if (border & UIViewBorderLeft)
	{
		BorderView *borderView = [self borderViewAtType:BorderViewTypeLeft];
		borderView.borderWidth = width;
	}
}

#pragma mark - Pricate Methods

- (BorderView *)borderViewAtType:(BorderViewType)type
{
	for (UIView *subview in self.subviews.copy)
	{
		if ([subview isKindOfClass:[BorderView class]])
		{
			BorderView *borderView = (BorderView *)subview;
			if (borderView.borderType == type)
			{
				return borderView;
			}
		}
	}
	
	return nil;
}

- (void)removeAllBorders
{
	for (UIView *subview in self.subviews.copy)
	{
		if ([subview isKindOfClass:[BorderView class]])
		{
			[subview removeFromSuperview];
		}
	}
}

@end

@interface BorderView ()

@property (nonatomic, assign) BorderViewType borderType;

@end

@implementation BorderView

#pragma mark - Life Cicle

- (void)dealloc
{
}

- (id)initWithType:(BorderViewType)type
{
	self = [self initWithType:type withColor:[UIColor blackColor]];
	
	if (self)
	{
		
	}
	
	return self;
}

- (id)initWithType:(BorderViewType)type withColor:(UIColor *)color
{
	self = [self initWithType:type withColor:color withWidth:0.5f];
	
	if (self)
	{
		
	}
	
	return self;
}

- (id)initWithType:(BorderViewType)type withColor:(UIColor *)color withWidth:(CGFloat)width
{
	self = [super initWithFrame:CGRectZero];
	
	self.borderWidth = width;
	self.backgroundColor = color;
	
	if (self)
	{
		self.borderType = type;
		switch (self.borderType)
		{
			case BorderViewTypeTop:
			{
				self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin);
				break;
			}
			case BorderViewTypeBottom:
			{
				self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
				break;
			}
			case BorderViewTypeLeft:
			{
				self.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin);
				break;
			}
			case BorderViewTypeRight:
			{
				self.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin);
				break;
			}
		}
	}
	
	return self;
}

#pragma mark - Property

- (void)setBorderColor:(UIColor *)borderColor
{
	self.backgroundColor = borderColor;
}

- (UIColor *)borderColor
{
	return self.backgroundColor;
}

#pragma mark - Private Methods

- (void)layout
{
	CGPoint contentOffset = CGPointZero;
	
	if ([self.superview isKindOfClass:[UIScrollView class]])
	{
		contentOffset = [(UIScrollView *)self.superview contentOffset];
	}
	
	switch (self.borderType)
	{
		case BorderViewTypeTop:
		{
			[super setFrame:CGRectMake(contentOffset.x,
									   contentOffset.y,
									   self.superview.frame.size.width,
									   self.borderWidth)];
			break;
		}
		case BorderViewTypeBottom:
		{
			[super setFrame:CGRectMake(contentOffset.x,
									   contentOffset.y + self.superview.frame.size.height - self.borderWidth,
									   self.superview.frame.size.width,
									   self.borderWidth)];
			break;
		}
		case BorderViewTypeLeft:
		{
			[super setFrame:CGRectMake(contentOffset.x,
									   contentOffset.y,
									   self.borderWidth,
									   self.superview.frame.size.height)];
			break;
		}
		case BorderViewTypeRight:
		{
			[super setFrame:CGRectMake(contentOffset.x + self.superview.frame.size.width - self.borderWidth,
									   contentOffset.y,
									   self.borderWidth,
									   self.superview.frame.size.height)];
			break;
		}
	}
}

#pragma mark - UIView Override Methods

- (void)setFrame:(CGRect)frame
{
	[self layout];
}

- (void)layoutSubviews
{
	[self.superview bringSubviewToFront:self];
	[self layout];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
	if ([newSuperview isKindOfClass:[UIScrollView class]])
	{
		UIScrollView *scrollView = (UIScrollView *)newSuperview;
		[scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
	}
	
	if ([self.superview isKindOfClass:[UIScrollView class]])
	{
		UIScrollView *scrollView = (UIScrollView *)self.superview;
		[scrollView removeObserver:self forKeyPath:@"contentOffset"];
	}
}

- (void)didMoveToSuperview
{
	[self layout];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"contentOffset"])
	{
		[self layout];
	}
}

@end