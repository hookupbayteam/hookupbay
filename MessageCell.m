//
//  MessageCell.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 25.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "MessageCell.h"
#import "Helper.h"

@implementation MessageCell

-(void)configureCell
{
    self.backgroundColor = UIColorFromRGB(0x9ed4e1);
    
    //define rect for message view
    
    if ([self.user isEqualToString:@"me"]) {
        self.messageView.alpha = 0.0;
        self.selfMessageView.alpha = 1.0;
        self.selfAvatarImage.image = self.avatar;
        self.selfMessageTextView.layer.cornerRadius = 2;
        self.selfMessageTextView.text = self.messageText;
        [self.selfMessageTextView setFont:[UIFont fontWithName:@"Trebuchet MS" size:13]];
        self.selfMessageTimeLabel.text = @"22:30 Fri";
    }
    else {
        self.messageView.alpha = 1.0;
        self.selfMessageView.alpha = 0.0;
        self.avatarImage.image = self.avatar;
        self.messageTextView.layer.cornerRadius = 2;
        self.messageTextView.text = self.messageText;
        [self.messageTextView setFont:[UIFont fontWithName:@"Trebuchet MS" size:13]];
        [self.messageTextView setTextColor:[UIColor whiteColor]];
        self.messageTimeLabel.text = @"22:30 Fri";
    }
}


@end
