//
//  ProfileImageView.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 25.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileImageView : UIImageView

@property (strong, nonatomic) UIImage *image;

@end
