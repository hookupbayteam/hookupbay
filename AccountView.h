//
//  AccountView.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 01.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountView : UIView

@property (strong, nonatomic) IBOutlet UIView *firstPhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *photo;


@property (strong, nonatomic) IBOutlet UIView *secondPhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *nextPhoto;

@property (strong, nonatomic) UISwipeGestureRecognizer *swipeRight;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeLeft;

@property (strong, nonatomic) NSDictionary *account;

-(void)updateUI;
@end
