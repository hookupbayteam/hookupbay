//
//  ProfilePictureCell.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 24.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileImageView.h"

@interface ProfilePictureCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet ProfileImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
@end
