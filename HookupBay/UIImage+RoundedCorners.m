//
//  UIImage+RoundedCorners.m
//  Aether
//
//  Created by Tatyana Mudryak on 01.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "UIImage+RoundedCorners.h"

@implementation UIImage (RoundedCorners)

+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original
{
	UIImageView *imageView = [[UIImageView alloc] initWithImage:original];

	// Begin a new image that will be the new image with the rounded corners
	// (here with the size of an UIImageView)
	UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 1.0);

	// Add a clip before drawing anything, in the shape of an rounded rect
	[[UIBezierPath bezierPathWithRoundedRect:imageView.bounds
								cornerRadius:cornerRadius] addClip];
	// Draw your image
	[original drawInRect:imageView.bounds];

	// Get the image, here setting the UIImageView image
	imageView.image = UIGraphicsGetImageFromCurrentImageContext();

	// Lets forget about that we were drawing
	UIGraphicsEndImageContext();

	return imageView.image;
}

+ (UIImage *)roundedImageWithImage:(UIImage *)original
{
	CGFloat radius = original.size.height / 2;
	return [self imageWithRoundedCornersSize:radius usingImage:original];
}

+ (UIImage *)imageByCropping:(UIImage *)image toSize:(CGSize)size
{
    double x = (image.size.width - size.width) / 2.0;
    double y = (image.size.height - size.height) / 2.0;
	
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
	
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
	
    return cropped;
}

+ (UIImage*)imageWithBorderFromImage:(UIImage*)source;
{
	CGSize size = [source size];
	UIGraphicsBeginImageContext(size);
	CGRect rect = CGRectMake(0, 0, size.width, size.height);
	[source drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(context, 1.0, 0.5, 1.0, 1.0);
	CGContextStrokeRect(context, rect);
	UIImage *testImg =  UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return testImg;
}

+ (UIImage *)ScaleImgPropoWidth:(UIImage *)image scaledToSize:(CGSize)newSize
{
	double ratio;
	double delta;
	//проверка на то если новый размер картинки больше или равен старому то вернуть ту же картинку
	if(newSize.width>=image.size.width) {
		return image;
	}
	
	ratio = newSize.width / image.size.width;
	delta = (ratio*image.size.height - ratio*image.size.width);
	UIImage *scaledImage =
	[UIImage imageWithCGImage:[image CGImage]
						scale:(image.scale / ratio)
				  orientation:(image.imageOrientation)];
	CGRect clipRect = CGRectMake(0, 0,
								 scaledImage.size.width,
								 scaledImage.size.height);
	CGSize sz = CGSizeMake(newSize.width, scaledImage.size.height);
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
		UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
	} else {
		UIGraphicsBeginImageContext(sz);
	}
	UIRectClip(clipRect);
	[image drawInRect:clipRect];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

+(UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation
{
	CGImageRef imgRef = initImage.CGImage;
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	UIImageOrientation orient = orientation;
	switch(orient) {
		case UIImageOrientationUp: //EXIF = 1
			return initImage; //если прислать в качестве параметра orientation – UIImageOrientationUp метод вернёт саму картинку целой и невредмой
			break;
		case UIImageOrientationUpMirrored: //EXIF = 2  – Зеркальное отображение
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
		case UIImageOrientationDown: //EXIF = 3 – перевернет вертикально
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
		case UIImageOrientationDownMirrored: //EXIF = 4 – Вертикальное зеркальное отображение
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
		case UIImageOrientationLeft: //EXIF = 6 – Перевернуть влево -90 градусов
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
		case UIImageOrientationRight: //EXIF = 8 – Вправо  +90 градусов по часовой стрелке
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
	}
	// Вот тут создается графический контекст
	CGContextRef    context = NULL;
	void *          bitmapData;
	int             bitmapByteCount;
	int             bitmapBytesPerRow;
	bitmapBytesPerRow   = (bounds.size.width * 4);
	bitmapByteCount     = (bitmapBytesPerRow * bounds.size.height);
	bitmapData = malloc( bitmapByteCount );
	if (bitmapData == NULL)
	{
		return nil;
	}
	CGColorSpaceRef colorspace = CGImageGetColorSpace(imgRef);
	context = CGBitmapContextCreate (bitmapData,bounds.size.width,bounds.size.height,8,bitmapBytesPerRow,
									 colorspace,kCGBitmapAlphaInfoMask );//ImageAlphaPremultipliedLast);
	CGColorSpaceRelease(colorspace);
	if (context == NULL)
		// если неполучилось создать контекст по какой либо причине отдатим nil
		return nil;
	CGContextScaleCTM(context, -1.0, -1.0);
	CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
	CGContextConcatCTM(context, transform);
	//Отрисовка изображения в графический контекст в памяти
	CGContextDrawImage(context, CGRectMake(0,0,width, height), imgRef);
	CGImageRef imgRef2 = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	free(bitmapData);
	UIImage * image = [UIImage imageWithCGImage:imgRef2 scale:initImage.scale orientation:UIImageOrientationUp];
	NSData *newimageData = UIImageJPEGRepresentation(image,1.0);
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:newimageData  forKey:@"chosed_image_data"];
	CGImageRelease(imgRef2);
	return image;
}


+ (UIImage *)imageWithFixedOrientationFromImage:(UIImage *)image
{
	
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
	
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
	
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
			
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
			
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
	
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
			
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
	
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
			
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
	
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
