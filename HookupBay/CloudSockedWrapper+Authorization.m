//
//  CloudSockedWrapper+Authorization.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper+Authorization.h"

@implementation CloudSockedWrapper (Authorization)


//step 4 - Auth user: attach session to socket
- (void)attachSessionWithID:(NSString *)sessionID
{
    currentAction = AttachSessionToSocket;
    messageID = [self generateUniqueMeesageID];
    NSDictionary *messageDict = @{@"id": connectionID,
                                  @"mid" : messageID,
                                  @"internal" : @"socketApi",
                                  @"data" : @{@"controller": @"auth",
                                              @"action" : @"user",
                                              @"params" : @{@"sid": sessionID}
                                              }
                                  };
    [self.socket sendJSON:messageDict];
}

- (NSString *)messageID
{
    if (!messageID) messageID = [[NSString alloc] init];
    return messageID;
}

- (NSString *)generateUniqueMeesageID
{
    messageID = [NSString stringWithFormat:@"%d", arc4random_uniform(100000)];
    return messageID;
}


@end
