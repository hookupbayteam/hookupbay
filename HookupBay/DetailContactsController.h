//
//  DetailContactsController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 25.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailContactsController : UITableViewController

@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSString *source;
@end
