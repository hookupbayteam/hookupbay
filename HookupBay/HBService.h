//
//  HBService.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudSockedWrapper.h"
#import "CloudSocket.h"

@protocol HBServiceDelegate <NSObject>

- (void)start;

@end

@interface HBService : NSObject <CloudSocketWrapperDelegate>


@property (nonatomic, strong) id <HBServiceDelegate> delegate;
@property (nonatomic, strong) CloudSockedWrapper *socketWrapper;

@end
