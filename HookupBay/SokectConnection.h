//
//  SokectConnection.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 19.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SocketConnectionDelegate <NSObject>

-(void)goChat;

@end


@interface SokectConnection : NSObject <NSStreamDelegate>

@property (strong, nonatomic) id <SocketConnectionDelegate> delegate;

- (void)initNetworkCommunication;
- (void)establishConnection;
@end
