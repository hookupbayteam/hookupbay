//
//  CloudSockedWrapper+Connection.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper+Connection.h"

@implementation CloudSockedWrapper (Connection)

//step 1 - establish connection
-(void)establishConnection;
{
    //currentAcion = OpenSocket;
    NSDictionary *messageDict = @{@"internal" : @"auth",
                                  @"data" : CONNECTION_DATA};
    [self.socket sendJSON:messageDict];
}

//step 2 - connect with connectionID
-(void)connectWithID
{
    //connection id, should be added to all further messages.
    //generated by client randomly once per connect, can contain any number of any symbols but try to make it unique
    isConnected = YES;
    currentAction = ConnectWithID;
    NSDictionary *messageDict = @{@"internal": @"connect",
                                  @"data" : @{@"main": @YES},
                                  @"id" : self.connectionID};
    
    [self.socket sendJSON:messageDict];
}

#pragma mark - Helpers

-(NSString *)connectionID
{
    if (!connectionID) {
        //Generates alpha-numeric-random string
        connectionID = [[NSString alloc] init];
        static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int len = 30;
        NSMutableString *randomString = [NSMutableString stringWithCapacity:len];
        for (int i = 0; i < len; i++) {
            [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
        }
        connectionID = randomString;
    }
    return connectionID;
}



@end
