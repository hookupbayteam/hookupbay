//
//  ChatUser.m
//  HookupBay
//
//  Created by Tatyana on 13.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ChatUser.h"

@implementation ChatUser

+ (ChatUser *)initWithFacebookID:(NSString *)fbID
{
   	ChatUser *user = [[ChatUser alloc] init];
    user.facebookId = fbID;
	return user;
}


@end
