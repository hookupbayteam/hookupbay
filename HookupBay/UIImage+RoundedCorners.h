//
//  UIImage+RoundedCorners.h
//  Aether
//
//  Created by Stepan Chepurin on 13/03/14.
//  Copyright (c) 2014 Technologika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImage (RoundedCorners)

+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original;
+ (UIImage *)roundedImageWithImage:(UIImage *)original;

+ (UIImage *)imageByCropping:(UIImage *)image toSize:(CGSize)size;

+ (UIImage*)imageWithBorderFromImage:(UIImage*)source;
+ (UIImage *)ScaleImgPropoWidth:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage*)imageByRotatingImage:(UIImage*)initImage fromImageOrientation:(UIImageOrientation)orientation;

+ (UIImage *)imageWithFixedOrientationFromImage:(UIImage *)image;
@end
