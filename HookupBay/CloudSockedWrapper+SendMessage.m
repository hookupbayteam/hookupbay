//
//  CloudSockedWrapper+SendMessage.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper+SendMessage.h"

@implementation CloudSockedWrapper (SendMessage)

-(void)sendChatMessage:(NSString *)text toUser:(NSNumber *)user
{
    currentAction = SendChatMessage;
    NSDictionary *messageDict = @{@"id": connectionID,
                                  @"mid" : messageID,
                                  @"internal" : @"socketApi",
                                  @"data" : @{@"controller": @"user",
                                              @"action" : @"sendMessage",
                                              @"params" : @{@"to" : [NSString stringWithFormat:@"\"%@\"", user],
                                                            @"text" : [NSString stringWithFormat:@"\"%@\"", text]}
                                              }
                                  };
    [self.socket sendJSON:messageDict];
}


@end
