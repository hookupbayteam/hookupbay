//
//  CloudSockedWrapper+History.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper+History.h"

@implementation CloudSockedWrapper (History)

-(void)loadConversationBetween:(NSNumber *)firstUser andUser:(NSNumber *)secondUser
{
  
}

-(void)loadHistory
{
    [self loadHistoryBetween:[NSNumber numberWithInt:4] andUser:[NSNumber numberWithInt:3]];
}

-(void)loadHistoryBetween:(NSNumber *)firstUser andUser:(NSNumber *)secondUser;
{
    currentAction = GetConversationHistory;
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSDictionary *messageDict = @{@"id": connectionID,
                                  @"internal" : @"history",
                                  @"channel" : [NSString stringWithFormat:@"presence-conversation-%@-%@", firstUser, secondUser],
                                  @"data" : @{@"skip": [NSNumber numberWithInt:0], // optional, default 0
                                              @"limit" : [NSNumber numberWithInt:0], // optional, default 20
                                              @"start" : [NSNumber numberWithInt:0], // timestamp date, optional, default 1 hour ago
                                              @"end" : timestamp} // current timestamp date, optional
                                  };
    [self.socket sendJSON:messageDict];
    
}


@end
