//
//  CloudSocket.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PORT 8080
#define HOST "54.83.43.55"
#define CONNECTION_DATA @"525ac96187f6479448000001"

@protocol CloudSocketDelegate <NSObject>

- (void)onConnect;
- (void)onDisconnect;
- (void)onJSONRecieved:(NSData *)json;

@end

@interface CloudSocket : NSObject <NSStreamDelegate>

@property (strong, nonatomic) id <CloudSocketDelegate> delegate;
@property (strong, nonatomic) NSInputStream *inputStream;
@property (strong, nonatomic) NSOutputStream *outputStream;

- (void)sendJSON:(NSDictionary *)json;
- (void)initNetworkCommunication;

@end
