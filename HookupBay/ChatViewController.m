//
//  ChatViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ChatViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "ChatViewController.h"
#import "UIImage+RoundedCorners.h"
#import "MessageCell.h"
#import "UIView+Border.h"

@interface ChatViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *messageTextField;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@end

@implementation ChatViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set left and right navigation button
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
   	UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self.revealViewController
                                                                  action:@selector(revealToggle:)];
    self.revealViewController.onlyPortrair = NO;
    MenuViewController *mvc = (MenuViewController *)self.revealViewController.rearViewController;
	mvc.source = self;
    
    menuButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = menuButton;

    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [self.messageTextField setBorder:UIViewBorderBottom withColor:UIColorFromRGB(0x4baaff) withWidth:0.5];
    
    //temporary data
    NSDictionary *m1 = @{@"text": @"How are you today",
                         @"avatar": [UIImage imageNamed:@"avatar1"],
                         @"user": @"me"};
    NSDictionary *m2 = @{@"text": @"Are you going to make me out like you promised or it is going to be like the last time?",
                         @"avatar": [UIImage imageNamed:@"avatar1"],
                         @"user" : @"me"};
    NSDictionary *m3 = @{@"text": @"Wiil do",
                         @"avatar": [UIImage imageNamed:@"avatar3"],
                         @"user" : @"1"};
    NSDictionary *m4 = @{@"text": @"I was there today also",
                         @"avatar": [UIImage imageNamed:@"avatar2"],
                         @"user" : @"1"};
    NSDictionary *m5 = @{@"text": @"Me too",
                         @"avatar": [UIImage imageNamed:@"avatar5"],
                         @"user" : @"1"};
    [self.messages addObject:m1];
    [self.messages addObject:m2];
    [self.messages addObject:m3];
    [self.messages addObject:m4];
    [self.messages addObject:m5];
}

-(NSMutableArray *)messages
{
    if (!_messages) _messages = [[NSMutableArray alloc] init];
    return _messages;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView datasource & delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatCell" forIndexPath:indexPath];
    
    NSDictionary *message = [self.messages objectAtIndex:indexPath.row];

    cell.messageText = message[@"text"];
    cell.avatar = message[@"avatar"];
    cell.user = message[@"user"];
    
    [cell configureCell];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView layoutSubviews];
}

#pragma mark - Actions

- (IBAction)tapToView:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (IBAction)sendMessage:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - Keyboard

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float keyboardOffset;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
        keyboardOffset = keyboardSize.width;
    else
        keyboardOffset = keyboardSize.height;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.mainView setFrame:CGRectMake(0, -keyboardOffset, SelfViewWidth, SelfViewHeight)];
                     }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.mainView setFrame:CGRectMake(0, 0, SelfViewWidth, SelfViewHeight)];
                     }];
}
@end
