//
//  CloudSocket.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSocket.h"

@implementation CloudSocket

- (void)initNetworkCommunication
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)CFSTR(HOST), PORT, &readStream, &writeStream);
    
    self.inputStream = (__bridge NSInputStream *)readStream;
    self.outputStream = (__bridge NSOutputStream *)writeStream;
    
    [self.inputStream setDelegate:self];
    [self.outputStream setDelegate:self];
    
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [self.inputStream open];
    [self.outputStream open];
}

- (void)sendJSON:(NSDictionary *)json
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    if (self.outputStream && self.outputStream.hasSpaceAvailable)
        [self.outputStream write:[data bytes] maxLength:[data length]];
}

#pragma mark - NSStream delegate methods

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent
{
    uint8_t buffer[1024];
    NSUInteger len;
    
    switch (streamEvent) {
        case NSStreamEventOpenCompleted:
            // NSLog(@"Stream opened now");
            [self.delegate onConnect];
            break;
            
        case NSStreamEventHasBytesAvailable:
            //  NSLog(@"has bytes");
            if (theStream == self.inputStream) {
                while ([self.inputStream hasBytesAvailable]) {
                    len = [self.inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        NSData *data = [[NSData alloc] initWithBytes:buffer length:len];
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSUTF8StringEncoding];
                        if (nil != output)
                            [self.delegate onJSONRecieved:data];
                    }
                }
            }
            break;
            
        case NSStreamEventHasSpaceAvailable:
            //  NSLog(@"Stream has space available now");
            break;
            
        case NSStreamEventErrorOccurred:
            //NSLog(@"Can not connect to the host!");
            [self.delegate onDisconnect];
            break;
            
        case NSStreamEventEndEncountered:
            [self.delegate onDisconnect];
            break;
            
        default:
            NSLog(@"Unknown event");
    }
}


@end
