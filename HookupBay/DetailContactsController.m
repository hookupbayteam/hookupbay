//
//  DetailContactsController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 25.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "DetailContactsController.h"

@interface DetailContactsController ()

@end

@implementation DetailContactsController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableArray *)contacts
{
    if (!_contacts) _contacts =  [[NSMutableArray alloc] init];
    return _contacts;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contacts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = UIColorFromRGB(0xffdbc2);
    [cell setSelectedBackgroundView:bgColorView];
    cell.textLabel.highlightedTextColor = UIColorFromRGB(0x484445);
    
    NSDictionary *contact = [self.contacts objectAtIndex:indexPath.row];
    
    if ([self.source isEqualToString:@"contact"]) {
        cell.imageView.image = contact[@"avatar"];
        cell.textLabel.text = contact[@"text"];
        cell.detailTextLabel.text = contact[@"name"];
    }
    
    if ([self.source isEqualToString:@"like"]) {
        cell.imageView.image = contact[@"avatar"];
        
        NSString *text = [NSString stringWithFormat:@"%@, %@", contact[@"name"], contact[@"age"]];
       
        UIColor *ageColor = UIColorFromRGB(0x8e8e8e);
        NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
								   ageColor, NSForegroundColorAttributeName, nil];
		NSRange range = [text rangeOfString:@","];
		if (!(range.location == NSNotFound)) {
				NSString *ageStr = [text substringFromIndex:range.location];
				NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
				[attributedText setAttributes:attrs range:[text rangeOfString:ageStr]];
				[cell.textLabel setAttributedText:attributedText];
			}
        
        cell.detailTextLabel.text = contact[@"city"];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
    
}
@end
