//
//  AccountPhotosViewController.h
//  HookupBay
//
//  Created by Tatyana on 30.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountPhotosViewController : UIViewController

@property (strong, nonatomic) UIImage *photo;
@property NSUInteger pageIndex;
@property NSUInteger totalPhotoCount;

-(id)initWithPhoto:(UIImage *)photo;
@end
