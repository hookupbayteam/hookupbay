//
//  ProfileViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ProfileViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "ProfilePictureCell.h"
#import "ProfileImageView.h"
#import "ChatViewController.h"
#import "UIView+Border.h"

@interface ProfileViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate>
{
    IBOutlet NSLayoutConstraint *textViewWidth;
    IBOutlet NSLayoutConstraint *buttonWidth;
    IBOutlet NSLayoutConstraint *collectionViewLeft;
    IBOutlet NSLayoutConstraint *addPictureViewTopDistance;
    IBOutlet NSLayoutConstraint *textViewTop;
    IBOutlet NSLayoutConstraint *collectionViewTopDistance;
    NSIndexPath *selectedItemIndexPath;
}

@property (strong, nonatomic) IBOutlet UITextView *mainTextView;
@property (strong, nonatomic) IBOutlet UIButton *peopleButton;
@property (strong, nonatomic) IBOutlet UIButton *plusButton;
@property (strong, nonatomic) IBOutlet UIButton *visitButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ProfileViewController

-(void)viewDidLoad
{
    [super viewDidLoad];

    //set left and right navigation button
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
	self.navigationController.navigationBar.translucent = NO;
    
	UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self.revealViewController
                                                                  action:@selector(revealToggle:)];
    self.revealViewController.onlyPortrair = NO;
    MenuViewController *mvc = (MenuViewController *)self.revealViewController.rearViewController;
	mvc.source = self;
    
    menuButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chats_button"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(goChat)];
    rightBarButtonItem.tintColor = UIColorFromRGB(0xff9f70);
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //temporary data
    for (int i = 1; i < 6; i++)
        [self.photos addObject:[UIImage imageNamed:@"noname"]];
    
    [self.collectionView reloadData];
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        [self prepareLandscapeWithDuration:0];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.mainTextView.layer.cornerRadius = 5.0;
    self.mainTextView.layer.borderColor = UIColorFromRGB(0xff9f70).CGColor;
    self.mainTextView.layer.borderWidth = 1.0;
    
    [self.peopleButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0xff9f70)] forState:UIControlStateHighlighted];
    [self.plusButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0xff9f70)] forState:UIControlStateHighlighted];
    [self.visitButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0xff9f70)] forState:UIControlStateHighlighted];
    [self.deleteButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0xff9f70)] forState:UIControlStateHighlighted];
    
    [self.peopleButton setBorder:UIViewBorderRight | UIViewBorderTop
                       withColor:UIColorFromRGB(0xb5b5b5)
                       withWidth:0.3];
    
    [self.plusButton setBorder:UIViewBorderRight | UIViewBorderTop
                       withColor:UIColorFromRGB(0xb5b5b5)
                       withWidth:0.3];
    
    [self.visitButton setBorder:UIViewBorderRight | UIViewBorderTop
                     withColor:UIColorFromRGB(0xb5b5b5)
                     withWidth:0.3];
    
    [self.deleteButton setBorder:UIViewBorderTop
                      withColor:UIColorFromRGB(0xb5b5b5)
                      withWidth:0.3];
    
    
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//lazy initialization
-(NSMutableArray *)photos
{
    if (!_photos) _photos = [[NSMutableArray alloc] init];
    return _photos;
}

#pragma mark - UICollectionView datasource & delegate methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.photos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ([cell isKindOfClass:[ProfilePictureCell class]]) {
        ProfileImageView *imageView = ((ProfilePictureCell *)cell).imageView;
        imageView.image = [self.photos objectAtIndex:indexPath.row];
    }
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    self.mainTextView.editable = NO;
    
    if (indexPath == selectedItemIndexPath) {
        [collectionView deselectItemAtIndexPath:selectedItemIndexPath animated:YES];
        selectedItemIndexPath = nil;
    } else {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        if  ([cell isKindOfClass:[ProfilePictureCell class]]) {
            [cell setSelected:YES];
            selectedItemIndexPath = indexPath;
        }
    }
}


-(CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [UIScreen mainScreen].bounds;
    
    // Adjust cell size for orientation
   if (UIDeviceOrientationIsLandscape([SharedApplication statusBarOrientation]))
   {
       float width = ((screenRect.size.height/2-40)/2)-2;
       return CGSizeMake( width, width*0.9);
   }
   else
       return CGSizeMake(137.f, 126.f);
}


#pragma mark - Actions

-(void)goChat
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    ChatViewController *chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"chat"];
    [self.navigationController pushViewController:chatViewController animated:YES];
}


- (IBAction)addPhoto:(id)sender
{
    
}
- (IBAction)editQuestion:(id)sender
{
    self.mainTextView.editable = !self.mainTextView.editable;
    if (self.mainTextView.editable)
        [self.mainTextView becomeFirstResponder];
    else
        [self.mainTextView resignFirstResponder];
}

#pragma mark - TextViewDelegate methods

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.mainTextView.editable = NO;
}

#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation))
        [self prepareLandscapeWithDuration:duration];
    else
        [self preparePortraitWithDuration:duration];
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
   [self.collectionView performBatchUpdates:nil completion:nil]; 
}


- (void)prepareLandscapeWithDuration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:duration
                     animations:^{
                         CGRect screenRect = [UIScreen mainScreen].bounds;
                         textViewWidth.constant = (screenRect.size.height / 2) - 20;
                         buttonWidth.constant = screenRect.size.height / 4;
                         addPictureViewTopDistance.constant = 45.0;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:duration
                     animations:^{
                         self.collectionView.alpha = 0.0;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration/2
                                          animations:^{
                                              collectionViewTopDistance.constant = 10.0;
                                              collectionViewLeft.constant = textViewWidth.constant + 30;
                                              [self.view layoutIfNeeded];
                                          } completion:^(BOOL finished) {
                                              self.collectionView.alpha = 1.0;
                                          }];
                     }];
}

- (void)preparePortraitWithDuration:(NSTimeInterval)duration
{
  
   [UIView animateWithDuration:duration
                     animations:^{
                         textViewWidth.constant =  280.0;
                         buttonWidth.constant = 80.0;
                         addPictureViewTopDistance.constant = 7.0;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:duration
                     animations:^{
                         self.collectionView.alpha = 0.0;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration/2
                                          animations:^{
                                              collectionViewTopDistance.constant = 135.0;
                                              collectionViewLeft.constant = 20.0;
                                               [self.view layoutIfNeeded];
                                          } completion:^(BOOL finished) {
                                              self.collectionView.alpha = 1.0;
                                          }];
                     }];
}

#pragma mark - Helper

-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

