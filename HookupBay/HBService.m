//
//  HBService.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "HBService.h"
#import "ServerAPI.h"
#import "CloudSockedWrapper.h"
#import "CloudSockedWrapper+Connection.h"
#import "CloudSockedWrapper+Authorization.h"
#import "CloudSockedWrapper+Subscribe.h"

@implementation HBService 

-(void)setSocketWrapper:(CloudSockedWrapper *)socketWrapper
{
    _socketWrapper = socketWrapper;
    _socketWrapper.delegate = self;
}

- (void)onConnectionEstablishing
{
    [self.socketWrapper connectWithID];
}

- (void)onConnectWithID
{
    //get Session ID
    [ServerAPI getSessionIdWithCompletionHandler:^(NSString *sessionID) {
        [self.socketWrapper attachSessionWithID:sessionID];
    }];
}

- (void)onSessionAttached
{
    [self.socketWrapper subscribeToUserPresence];
}

- (void)onUserPresenceSubscribed
{
    [self.socketWrapper subscribeToPrivateMessage];
}

- (void)onPrivateMessageSubscribed
{
    [self.delegate start];
}

- (void)onMessageHistoryLoaded
{
    
}

@end
