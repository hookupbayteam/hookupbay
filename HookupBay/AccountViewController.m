//
//  AccountViewController.m
//  HookupBay
//
//  Created by Tatyana on 29.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "AccountViewController.h"
#import "AccountPhotosViewController.h"
#import "UIHelper.h"

@interface AccountViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray *photos;

@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextView *questionLabel;
@property (strong, nonatomic) IBOutlet UITextView *questionTextView;
@end

@implementation AccountViewController

-(id)initWithAccount:(NSDictionary *)account
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"AccountViewController"];
	if (self)
	{
        if (account) {
            self.account = account;
            self.photos = account[@"photos"];
        }
	}
	return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //fill outlets for first account
    self.photoView.image = [self.account[@"photos"] objectAtIndex:0];
    NSString *text = [NSString stringWithFormat:@"%@, %@", self.account[@"name"], self.account[@"age"]];
    
    UIColor *ageColor = UIColorFromRGB(0x8e8e8e);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           ageColor, NSForegroundColorAttributeName, nil];
    NSRange range = [text rangeOfString:@","];
    if (!(range.location == NSNotFound)) {
        NSString *ageStr = [text substringFromIndex:range.location];
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
        [attributedText setAttributes:attrs range:[text rangeOfString:ageStr]];
    }
    self.nameLabel.text = text;
    self.questionLabel.text = self.account[@"question"];
    self.questionTextView.contentInset = UIEdgeInsetsMake(-3,-5,0,0);
}

-(NSMutableArray *)photos
{
    if (!_photos) _photos = [[NSMutableArray alloc] init];
    return _photos;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PageViewController datasource methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((AccountPhotosViewController *) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((AccountPhotosViewController *) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.photos count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (AccountPhotosViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.photos count] == 0) || (index >= [self.photos count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    AccountPhotosViewController *photoViewController = [[AccountPhotosViewController alloc] initWithPhoto:[self.photos objectAtIndex:index]];
    photoViewController.pageIndex = index;
    photoViewController.totalPhotoCount = [self.photos count];
    return photoViewController;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPhotos"]) {
        UIPageViewController *pvc = segue.destinationViewController;
        pvc.delegate = self;
        pvc.dataSource = self;
        
        AccountPhotosViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pvc setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
}




@end
