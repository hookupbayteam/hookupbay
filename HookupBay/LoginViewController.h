//
//  LoginViewController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBService.h"

@interface LoginViewController : UIViewController <HBServiceDelegate>

@end
