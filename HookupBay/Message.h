//
//  Message.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 11.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *jid;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDate *date;

@end
