//
//  LikeListViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 28.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "LikeListViewController.h"
#import "DetailContactsController.h"

@interface LikeListViewController ()

@property (strong, nonatomic) NSMutableArray *contacts;

@end

@implementation LikeListViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"like"]) {
        DetailContactsController *dvc = segue.destinationViewController;
        [self loadContactList];
        dvc.contacts = self.contacts;
        dvc.source = @"like";
    }
}

-(void)loadContactList
{
    self.contacts = [[NSMutableArray alloc] init];
    NSDictionary *m1 = @{@"name": @"Adina White",
                         @"city": @"Bucharest",
                         @"avatar": [UIImage imageNamed:@"avatar1"],
                         @"age": [NSNumber numberWithInt:22]};
    
    NSDictionary *m2 = @{@"name": @"Polina Cupid",
                         @"city": @"New Jersey",
                         @"avatar": [UIImage imageNamed:@"avatar3"],
                         @"age": [NSNumber numberWithInt:21]};
    
    NSDictionary *m3 = @{@"name": @"Adina White",
                         @"city": @"Bucharest",
                         @"avatar": [UIImage imageNamed:@"avatar3"],
                         @"age": [NSNumber numberWithInt:26]};
    
    NSDictionary *m4 = @{@"name": @"Polina Cupid",
                         @"city": @"New York",
                         @"avatar": [UIImage imageNamed:@"avatar4"],
                         @"age": [NSNumber numberWithInt:18]};
    
    NSDictionary *m5 = @{@"name": @"Adina White",
                         @"city": @"Bucharest",
                         @"avatar": [UIImage imageNamed:@"avatar1"],
                         @"age": [NSNumber numberWithInt:34]};
    
    
    [self.contacts addObject:m1];
    [self.contacts addObject:m2];
    [self.contacts addObject:m3];
    [self.contacts addObject:m4];
    [self.contacts addObject:m5];
}


@end
