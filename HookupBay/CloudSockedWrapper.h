//
//  CloudSockedWrapper.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudSocket.h"

@protocol CloudSocketWrapperDelegate <NSObject>

- (void)onConnectionEstablishing;
- (void)onConnectWithID;
- (void)onSessionAttached;
- (void)onUserPresenceSubscribed;
- (void)onPrivateMessageSubscribed;
- (void)onMessageHistoryLoaded;

@end

typedef enum {
    OpenSocket,
    ConnectWithID,
    AttachSessionToSocket,
    GetChannelAuthString,
    GetSubscribeResult,
    GetConversationHistory,
    SendChatMessage
} Actions;

@interface CloudSockedWrapper : NSObject <CloudSocketDelegate>
{
    BOOL isConnected;
    BOOL isSessionActive;
    Actions currentAction;
    
    NSString *connectionID;
    NSString *messageID;
    
    NSNumber *myUserId;
    NSString *userName;
    
    NSString *userEmail;
    NSString *userPassword;
    NSString *serverResponse;
    
    NSString *authChannelString;
    NSString *channelID;
    NSDictionary *channelData;
    
    NSDictionary *privateMessage;
    NSDictionary *messageHistory;
    NSString *messageDateStamp;
    
}

@property (strong, nonatomic) CloudSocket *socket;
@property (strong, nonatomic) id <CloudSocketWrapperDelegate> delegate;


@end
