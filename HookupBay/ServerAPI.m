//
//  ServerAPI.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ServerAPI.h"

@implementation ServerAPI

+(void)getSessionIdWithCompletionHandler:(SessionIdCompletionHandler)completionHandler
{
    //wait for 'PHPSESSID' cookie in response (or 'sid' field in last development version)
    
    NSString *bodyStr = [NSString stringWithFormat:@"{\"username\": \"%@\", \"password\": \"%@\"}", temp_login, temp_pass];
    NSData *body = [bodyStr dataUsingEncoding:NSUTF8StringEncoding];
	NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[bodyStr length]];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGIN_HOST]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"POST"];
	[request setHTTPBody:body];
    [request setHTTPShouldHandleCookies:YES];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (!connectionError) {
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
                                   NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:[NSURL URLWithString:LOGIN_HOST]];
                                   for (NSHTTPCookie *cookie in cookies) {
                                       if ([cookie.name isEqualToString:@"PHPSESSID"] || [cookie.name isEqualToString:@"sid"]) {
                                          completionHandler(cookie.value);
                                       }
                                   }
                               } else
                                   NSLog(@"%@", connectionError.localizedDescription);
                           }];
    
}


@end
