//
//  QuestionsViewController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionsViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *accounts;

@end
