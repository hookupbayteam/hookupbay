//
//  LikesViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 28.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "LikesViewController.h"
#import "SWRevealViewController.h"
#import "ChatViewController.h"
#import "AnimationTabbarViewController.h"
#import "MenuViewController.h"

@interface LikesViewController () <UITabBarControllerDelegate, UIViewControllerTransitioningDelegate, UIGestureRecognizerDelegate>

@end

@implementation LikesViewController {
    AnimationTabbarViewController *animationController;
    BOOL sideMenuOpen;
}


-(void)viewDidLoad
{
    [super viewDidLoad];
   
    self.delegate = self;
    animationController = [AnimationTabbarViewController new];
    animationController.duration = 0.3;

    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
	self.navigationController.navigationBar.translucent = NO;
    
	UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self.revealViewController
                                                                  action:@selector(revealToggle:)];
    self.revealViewController.onlyPortrair = NO;
    MenuViewController *mvc = (MenuViewController *)self.revealViewController.rearViewController;
	mvc.source = self;
    
    menuButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chats_button"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(goChat)];
    rightBarButtonItem.tintColor = UIColorFromRGB(0xff9f70);
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    // Set the gesture
    //[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFromRight:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    swipeLeft.delegate = self;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeFromLeft:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    swipeRight.delegate = self;
    [self.view addGestureRecognizer:swipeRight];

    sideMenuOpen = NO;
    
    //configure tabbar
    [self.tabBar setBackgroundColor:[UIColor whiteColor]];
    
    UIImage *selectedImage, *unselectedImage = nil;
    for (UITabBarItem *item in self.tabBar.items)
    {
        switch (item.tag)
        {
            case 0:
            {
                unselectedImage = [UIImage imageNamed:@"like"];
                selectedImage = [UIImage imageNamed:@"like"];
            }
                break;
                
            case 1:
            {
                unselectedImage = [UIImage imageNamed:@"favourite"];
                selectedImage = [UIImage imageNamed:@"favourite"];
            }
                break;
                
            case 2:
            {
                unselectedImage = [UIImage imageNamed:@"fire"];
                selectedImage = [UIImage imageNamed:@"fire"];
            }
                break;
                
            case 3:
            {
                unselectedImage = nil;
                selectedImage = nil;
                item.enabled = NO;
            }
                break;
                
            default:
                break;
        }
        
        item.image = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        if (IS_IPHONE_5)
            [self.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button_5_land"]];
        else
            [self.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button_land"]];
        [self.tabBar setBackgroundImage:[UIImage imageNamed:@"back_lines_land"]];
    } else {
        [[UITabBar appearance] setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button"]];
        [self.tabBar setBackgroundImage:[UIImage imageNamed:@"back_lines"]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goChat
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    ChatViewController *chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"chat"];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

#pragma mark - Gesture

- (IBAction)swipeFromRight:(id)sender
{
    CGPoint touchPoint = [sender locationInView:self.view];
    CGRect touchRect;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        if (sideMenuOpen)
            touchRect = CGRectMake(SelfViewWidth-200, 0, 200, SelfViewHeight);
        else
            touchRect = CGRectMake(50, 0, SelfViewWidth, SelfViewHeight);
        
    } else {
        if (sideMenuOpen)
            touchRect = CGRectMake(SelfViewWidth-100, 0, 100, SelfViewHeight);
        else
            touchRect = CGRectMake(50, 0, SelfViewWidth, SelfViewHeight);
    }
    
    BOOL touchForSwipe = CGRectContainsPoint(touchRect, touchPoint);
    
    if (touchForSwipe) {
        //change tab
        NSUInteger selectedIndex = [self selectedIndex];
         selectedIndex == 2 ? [self setSelectedIndex:selectedIndex] : [self setSelectedIndex:selectedIndex + 1];
        sideMenuOpen = NO;
    } else {
        //close menu
        [self.revealViewController revealToggle:self];
        sideMenuOpen = YES;
    }
    
    
}

- (IBAction)swipeFromLeft:(UIGestureRecognizer *)sender
{
    CGPoint touchPoint = [sender locationInView:self.view];
    CGRect touchRect;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        if (sideMenuOpen)
            touchRect = CGRectMake(SelfViewHeight-200, 0, 200, SelfViewWidth);
        else
            touchRect = CGRectMake(50, 0, SelfViewHeight, SelfViewWidth);
        
    } else {
        if (sideMenuOpen)
            touchRect = CGRectMake(0, 0, SelfViewWidth-50, SelfViewHeight);
        else
            touchRect = CGRectMake(50, 0, SelfViewWidth, SelfViewHeight);
    }
    
    BOOL touchForSwipe = CGRectContainsPoint(touchRect, touchPoint);
    
    if (touchForSwipe) {
        //change tab
        NSUInteger selectedIndex = [self selectedIndex];
        [self setSelectedIndex:selectedIndex - 1];
        sideMenuOpen = NO;
    } else {
        //open menu
        [self.revealViewController revealToggle:self];
        sideMenuOpen = YES;
    }
    
}


#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation)) {
        if (IS_IPHONE_5)
            [self.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button_5_land"]];
        else
            [self.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button_land"]];
        [self.tabBar setBackgroundImage:[UIImage imageNamed:@"back_lines_land"]];
    } else {
        [self.tabBar setSelectionIndicatorImage: [UIImage imageNamed:@"selected_button"]];
        [self.tabBar setBackgroundImage:[UIImage imageNamed:@"back_lines"]];
    }
}

#pragma mark - Tab bar animation

- (id <UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController
            animationControllerForTransitionFromViewController:(UIViewController *)fromVC
                                              toViewController:(UIViewController *)toVC {
    
    NSUInteger fromVCIndex = [tabBarController.viewControllers indexOfObject:fromVC];
    NSUInteger toVCIndex = [tabBarController.viewControllers indexOfObject:toVC];
    
    animationController.reverse = fromVCIndex > toVCIndex;
    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    animationController.isPresenting = NO;
    return animationController;
}


@end
