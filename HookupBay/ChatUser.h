//
//  ChatUser.h
//  HookupBay
//
//  Created by Tatyana on 13.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatUser : NSObject

@property (nonatomic, strong) NSString *facebookId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) NSArray *photos;

+ (ChatUser *)initWithFacebookID:(NSString *)fbID;
@end
