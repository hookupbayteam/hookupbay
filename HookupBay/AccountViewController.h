//
//  AccountPhotoViewController.h
//  HookupBay
//
//  Created by Tatyana on 29.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountViewController : UIViewController

@property (strong, nonatomic) NSDictionary *account;
@property NSUInteger pageIndex;

-(id)initWithAccount:(NSDictionary *)account;
@end
