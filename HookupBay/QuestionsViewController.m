//
//  QuestionsViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "QuestionsViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "ChatViewController.h"
#import "AccountView.h"
#import "UIView+Border.h"
#import <objc/message.h>

@interface QuestionsViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>
{
    NSMutableArray *photos;
    NSInteger nextPageIndex; //page which will display next
    NSInteger activeView; //view which is now displayed: 1 - accountView, 2 - nextAccountView
    BOOL lastAccount;
    BOOL sideMenuOpen;
    BOOL hideAccount;
}

@property (strong, nonatomic) UIPanGestureRecognizer *dragAccount;
@property (strong, nonatomic) UIPanGestureRecognizer *dragNextAccount;

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *accountsView;
@property (strong, nonatomic) AccountView *accountView;
@property (strong, nonatomic) AccountView *nextAccountView;

@property (strong, nonatomic) IBOutlet UITextField *answerTextView;
@property (strong, nonatomic) IBOutlet UIView *searchView;

@property (strong, nonatomic) IBOutlet UIView *buttonsView;
@property (strong, nonatomic) IBOutlet UIButton *noButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *answerButton;

@end


@implementation QuestionsViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set left and right navigation button
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
	self.navigationController.navigationBar.translucent = NO;
    
	UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self.revealViewController
                                                                  action:@selector(revealToggle:)];
    self.revealViewController.onlyPortrair = YES;
    MenuViewController *mvc = (MenuViewController *)self.revealViewController.rearViewController;
	mvc.source = self;
    
    menuButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chats_button"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(goChat)];
    rightBarButtonItem.tintColor = UIColorFromRGB(0xff9f70);
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    [self.answerTextView setBorder:UIViewBorderBottom withColor:UIColorFromRGB(0xeea546) withWidth:1.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadAccountsData];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //set the orientation for this view controller is only portrait
    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)loadAccountsData
{
    //temporary data
    if (!self.accounts)
    {
        self.accounts = [[NSMutableArray alloc] init];
        NSDictionary *account1 = @{@"name": @"Bridgid_1",
                                   @"age": [NSNumber numberWithInt:20],
                                   @"question":@"What is you favourite book?",
                                   @"photos":@[[UIImage imageNamed:@"photo"], [UIImage imageNamed:@"photo2"], [UIImage imageNamed:@"photo"]]};
        
        NSDictionary *account2 = @{@"name": @"Masha_2",
                                   @"age": [NSNumber numberWithInt:25],
                                   @"question":@"How would you water you favourite flowers in a summer afternoon if you had to dress in red and wear high heels?",
                                   @"photos":@[[UIImage imageNamed:@"photo2"], [UIImage imageNamed:@"photo"]]};
        
        NSDictionary *account3 = @{@"name": @"Bridgid_3",
                                   @"age": [NSNumber numberWithInt:20],
                                   @"question":@"What is you favourite book?",
                                   @"photos":@[[UIImage imageNamed:@"photo"], [UIImage imageNamed:@"photo2"]]};
        
        NSDictionary *account4 = @{@"name": @"Masha_4",
                                   @"age": [NSNumber numberWithInt:25],
                                   @"question":@"How would you water you favourite flowers in a summer afternoon if you had to dress in red and wear high heels?",
                                   @"photos":@[[UIImage imageNamed:@"photo2"], [UIImage imageNamed:@"photo"]]};
        
        [self.accounts addObject:account1];
        [self.accounts addObject:account2];
        [self.accounts addObject:account3];
        [self.accounts addObject:account4];
    }
    
    nextPageIndex = 0;
    lastAccount = NO;
    activeView = 1;
    
    //initial first two account view
    self.accountView = [[[NSBundle mainBundle] loadNibNamed:@"AccountView" owner:self options:nil] objectAtIndex:0];
    self.nextAccountView = [[[NSBundle mainBundle] loadNibNamed:@"AccountView" owner:self options:nil] objectAtIndex:0];
    
    [self updateAccountView:self.accountView withIndex:nextPageIndex];
    [self updateAccountView:self.nextAccountView withIndex:nextPageIndex];
    
    [self.accountsView addSubview:self.nextAccountView];
    [self.accountsView addSubview:self.accountView];
    
    
    // Set the gesture
    //  [self.accountView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.dragAccount= [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragAccountView:)];
    self.dragAccount.delegate = self;
    [self.accountView addGestureRecognizer:self.dragAccount];
    
    self.dragNextAccount = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragAccountView:)];
    self.dragNextAccount.delegate = self;
    [self.nextAccountView addGestureRecognizer:self.dragNextAccount];
    
    [self.dragAccount requireGestureRecognizerToFail:self.accountView.swipeRight];
    [self.dragAccount requireGestureRecognizerToFail:self.accountView.swipeLeft];
    
    [self.dragNextAccount requireGestureRecognizerToFail:self.nextAccountView.swipeRight];
    [self.dragNextAccount requireGestureRecognizerToFail:self.nextAccountView.swipeLeft];
    // [self.revealViewController.panGestureRecognizer requireGestureRecognizerToFail:self.swipePhotos];
}


#pragma mark - Actions

-(void)goChat
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    ChatViewController *chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"chat"];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

- (IBAction)sendQuestion:(id)sender
{
    [self.view endEditing:YES];
    self.buttonsView.alpha = 1.0;
}

- (IBAction)writeAnAnswer:(id)sender
{
    self.buttonsView.alpha = 0.0;
    [self.answerTextView becomeFirstResponder];
}

- (IBAction)likeClicked:(id)sender
{
    if (!lastAccount)
        [self showNextAccountAnimated:YES];
}

- (IBAction)noClicked:(id)sender
{
    if (!lastAccount)
        [self showNextAccountAnimated:YES];
}

-(void)showNextAccountAnimated:(BOOL)animated
{
    if (activeView == 1)
       [self changeAccountViewWithButton:self.accountView];
    else
       [self changeAccountViewWithButton:self.nextAccountView];
 
}

#pragma mark - Gestures

- (IBAction)dragAccountView:(UIPanGestureRecognizer *)sender
{
   //show next account
   CGPoint translation = [sender translationInView:sender.view];
   sender.view.center = CGPointMake(sender.view.center.x + translation.x, sender.view.center.y + translation.y);
    
   //calculate slideFactop for smooth animation
   CGPoint velocity = [sender velocityInView:self.view];
   CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
   CGFloat slideMult = magnitude / 200;
   float slideFactor = 0.1 * slideMult;
    
    
    
    //found direction
    if (velocity.y > 0) { //down
        if (velocity.x > 50) {
            //swipe right down
            [self.noButton setHighlighted:YES];
            [self.likeButton setHighlighted:NO];
            hideAccount = YES;
        } else if (velocity.x < -50) {
            //swipe left down
            [self.likeButton setHighlighted:YES];
            [self.noButton setHighlighted:NO];
            hideAccount = YES;
        }
    } else { //up
        hideAccount = NO;
        [self.noButton setHighlighted:NO];
        [self.likeButton setHighlighted:NO];
    }

 
    if (sender.state == UIGestureRecognizerStateRecognized) {
        [UIView animateWithDuration: slideFactor
                              delay: 0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             sender.view.center = CGPointMake(sender.view.center.x + translation.x,
                                                              sender.view.center.y + translation.y);
                         }
                         completion:nil];
    }
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        if (hideAccount && !lastAccount) {
            [self changeAccountViewWithGesture:(AccountView *)sender.view];
            [self.noButton setHighlighted:NO];
            [self.likeButton setHighlighted:NO];
        } else {
            //back current account to origin position
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 sender.view.frame = self.accountsView.frame;
                             }
                             completion:nil];
        }
    }
    [sender setTranslation:CGPointMake(0, 0) inView:sender.view];
}

-(void)changeAccountViewWithGesture:(AccountView *)view
{
    //hide view and clear
    view.alpha = 0.0;
    
    //reposition
    view.frame = self.accountsView.frame;
    if ([view isEqual:self.accountView]) {
        [self.accountsView insertSubview:self.nextAccountView aboveSubview:self.accountView];
        activeView = 2;
    }
    if ([view isEqual:self.nextAccountView]) {
        [self.accountsView insertSubview:self.accountView aboveSubview:self.nextAccountView];
        activeView = 1;
    }
    
    //load next data
    [self updateAccountView:view withIndex:nextPageIndex];
    if (!lastAccount)
       view.alpha = 1.0;
}

-(void)changeAccountViewWithButton:(AccountView *)view
{
   //  view.alpha = 0.0;
    
    [self updateAccountView:view withIndex:nextPageIndex+1];
   
    if ([view isEqual:self.accountView]) {
        [self.nextAccountView setFrame:CGRectMake(320, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
        [UIView animateWithDuration:0.3
                         animations:^{
                             [self.accountView setFrame:CGRectMake(-320, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
                             [self.nextAccountView setFrame:CGRectMake(0, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
                         }
                         completion:^(BOOL finished) {
                             self.accountView.frame = self.accountsView.frame;
                             [self.accountsView insertSubview:self.nextAccountView aboveSubview:self.accountView];
                             activeView = 2;
                         }];
    }
    
    if ([view isEqual:self.nextAccountView]) {
        [self.accountView setFrame:CGRectMake(320, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
        [UIView animateWithDuration:0.3
                         animations:^{
                             [self.nextAccountView setFrame:CGRectMake(-320, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
                             [self.accountView setFrame:CGRectMake(0, 0, self.accountsView.frame.size.width, self.accountsView.frame.size.height)];
                         }
                         completion:^(BOOL finished) {
                             self.nextAccountView.frame = self.accountsView.frame;
                             [self.accountsView insertSubview:self.accountView aboveSubview:self.nextAccountView];
                             activeView = 1;
                         }];
    }
    
//    if (!lastAccount)
//        view.alpha = 1.0;
}

-(void)updateAccountView:(AccountView *)view withIndex:(NSInteger)index
{
    if (index+1 > self.accounts.count) {
        lastAccount = YES;
    } else {
        view.account = [self.accounts objectAtIndex:nextPageIndex];
        view.frame = self.accountsView.frame;
        nextPageIndex++;
        [view updateUI];
    }
}


#pragma mark - Keyboard notifications

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float keyboardOffset;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
        keyboardOffset = keyboardSize.width;
    else
        keyboardOffset = keyboardSize.height;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.mainView setFrame:CGRectMake(0, -keyboardOffset, self.mainView.frame.size.width, self.mainView.frame.size.height)];
                     }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.mainView setFrame:CGRectMake(0, 0, self.mainView.frame.size.width, self.mainView.frame.size.height)];
                     }];
}


@end
