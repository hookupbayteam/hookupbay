//
//  ProfileViewController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ChatUser.h"

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) ChatUser *currentUser;
@property (strong, nonatomic) NSMutableArray *photos;

@end
