//
//  ChatContactsViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 28.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ChatContactsViewController.h"
#import "DetailContactsController.h"

@interface ChatContactsViewController ()

@property (strong, nonatomic) NSMutableArray *contacts;

@end

@implementation ChatContactsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chatContacts"]) {
        DetailContactsController *dvc = segue.destinationViewController;
        [self loadContactList];
        dvc.contacts = self.contacts;
        dvc.source = @"contact";
    }
}

-(void)loadContactList
{
    self.contacts = [[NSMutableArray alloc] init];
    NSDictionary *m1 = @{@"name": @"Adina White",
                         @"text": @"Hey",
                         @"avatar": [UIImage imageNamed:@"avatar3"]};
    NSDictionary *m2 = @{@"name": @"Ashly Fire",
                         @"text": @"Salut :)",
                         @"avatar": [UIImage imageNamed:@"avatar4"]};
    NSDictionary *m3 = @{@"name": @"Dorian Lucian",
                         @"text": @"Call me",
                         @"avatar": [UIImage imageNamed:@"avatar1"]};
    NSDictionary *m4 = @{@"name": @"Samantha Jones",
                         @"text": @"some thanks",
                         @"avatar": [UIImage imageNamed:@"avatar4"]};
    NSDictionary *m5 = @{@"name": @"Polina Cupid",
                         @"text": @"Kiss bye",
                         @"avatar": [UIImage imageNamed:@"avatar1"]};
    NSDictionary *m6 = @{@"name": @"Bogdan",
                         @"text": @"...",
                         @"avatar": [UIImage imageNamed:@"avatar4"]};
    
    
    [self.contacts addObject:m1];
    [self.contacts addObject:m2];
    [self.contacts addObject:m3];
    [self.contacts addObject:m4];
    [self.contacts addObject:m5];
    [self.contacts addObject:m6];
}


@end
