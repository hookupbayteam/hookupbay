//
//  LoginViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "LoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "ChatUser.h"
#import "UIImage+RoundedCorners.h"
#import "UIView+Border.h"
#import "CloudSocket.h"
#import "CloudSockedWrapper.h"

#define temp_login @"francia@gmail.com"
#define temp_pass @"123456"

@interface LoginViewController () <UITextFieldDelegate>
{
    BOOL checkboxSelected;
}

@property (strong, nonatomic) HBService *service;

@property (weak, nonatomic) IBOutlet UIButton *checkbox;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

#pragma mark - HBServerConnectionDelegate methods

-(void)start
{
  [self performSegueWithIdentifier:@"connect" sender:self];
}

-(void)noConnection
{
  [self showMessage:@"No internet connection!" withTitle:@"Error"];
}

#pragma mark - Actions

- (IBAction)checkboxClicked:(id)sender
{
    _checkbox.selected = !_checkbox.selected;
}

- (IBAction)login:(id)sender
{
    CloudSocket *socket = [[CloudSocket alloc] init];
    CloudSockedWrapper *socketWrapper = [[CloudSockedWrapper alloc] init];
    [socketWrapper setSocket:socket];
    HBService *service = [[HBService alloc] init];
    [service setSocketWrapper:socketWrapper];
    
    [socket initNetworkCommunication];
}

- (IBAction)loginViaFacebook:(id)sender
{
  //  [self loginViaFacebook];
}

#pragma mark - Facebook login

-(void)loginViaFacebook
{
    [self.spinner startAnimating];
 
    // Whenever a person opens the app, check for a cached session
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // This method will be called EACH time the session state changes,
                                          // also for intermediate states and NOT just when the session open
                                           [self sessionStateChanged:session state:state error:error];
                                      }];
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Call the sessionStateChanged:state:error method to handle session state changes
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }
}

// This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen) {
        NSLog(@"Facebook session opened");
        // Get user info and show the logged-in UI
        [self getFacebookUserInfo];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed) {
        // If the session is closed
        NSLog(@"Facebook session closed");
        // Show the logged-out UI
        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
}

-(void)getFacebookUserInfo
{
    [[FBRequest requestForMe] startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                            NSDictionary<FBGraphUser> *user,
                                                            NSError *error) {
         if (!error) {
             ChatUser *currentUser = [ChatUser initWithFacebookID:user.objectID];
             currentUser.firstName = user.first_name;
             currentUser.lastName = user.last_name;
             currentUser.facebookId = user.objectID;
             currentUser.gender = [user objectForKey:@"gender"];
             currentUser.email = [user objectForKey:@"email"] ? [user objectForKey:@"email"] : @"";
             //create avatar - need to be in the other place!!!
//             NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", user.objectID]]];
//             UIImage *image = [UIImage imageWithData:imageData];
//             image = [UIImage imageByCropping:image toSize:CGSizeMake(82, 82)];
//             //image = [UIImage imageWithRoundedCornersSize:40.0 usingImage:image];
//             currentUser.avatar = image;
            
             
             [self userLoggedIn];
         }
     }];
}

// Show the user the logged-out UI
- (void)userLoggedOut
{
    [self showMessage:@"You're now logged out" withTitle:@""];
}

// Show the user the logged-in UI
- (void)userLoggedIn
{
 
  // [self performSegueWithIdentifier:@"connect" sender:self];
}

-(void)didAuthenticate
{
    [self.spinner stopAnimating];
    //[self performSegueWithIdentifier:@"connect" sender:self];
}

-(void)didNotAuthenticate
{
  [self.spinner stopAnimating];
  [self showMessage:@"Incorrect username or password" withTitle:@"Authentication error"];
}

// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}

#pragma mark - Navigation

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"connect"]) {
//
//    }
//}

@end














