//
//  CloudSockedWrapper+SendMessage.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper.h"

@interface CloudSockedWrapper (SendMessage)

-(void)sendChatMessage:(NSString *)text toUser:(NSNumber *)user;

@end
