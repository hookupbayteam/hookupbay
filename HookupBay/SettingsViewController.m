//
//  SettingsViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "ChatViewController.h"
#import "NMRangeSlider.h"
#import "UIView+Border.h"

@interface SettingsViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>
{
    BOOL isWoman;
    BOOL isMen;
    BOOL vibrateOn;
    BOOL soundOn;
    int minAge;
    int maxAge;
    int distance;
    
    IBOutlet NSLayoutConstraint *distanceSliderWidth;
    IBOutlet NSLayoutConstraint *distanceSliderHeight;
    IBOutlet NSLayoutConstraint *ageSliderLeft;
    IBOutlet NSLayoutConstraint *ageSliderTop;
}

@property (weak, nonatomic) IBOutlet UIButton *womenCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *menCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *vibrateCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *soundCheckBox;

@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectCityLabel;

@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet NMRangeSlider *ageSlider;
@property (strong, nonatomic) IBOutlet NMRangeSlider *distanceSlider;
@property (strong, nonatomic) IBOutlet UIImageView *distanceSliderValueView;
@property (strong, nonatomic) IBOutlet UILabel *distanceSliderValueLabel;


@property (strong, nonatomic) IBOutlet UIView *secondSliderView;
@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     //set left and right navigation button
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;

    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal_icon"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self.revealViewController
                                                                  action:@selector(revealToggle:)];
    self.revealViewController.onlyPortrair = NO;
    MenuViewController *mvc = (MenuViewController *)self.revealViewController.rearViewController;
	mvc.source = self;
    
    menuButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chats_button"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(goChat)];
    rightBarButtonItem.tintColor = UIColorFromRGB(0xff9f70);
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;

    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.revealViewController.panGestureRecognizer.delegate = self;
    
    //sliders
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"slider_button"] forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackTintColor:UIColorFromRGB(0x6f6f6f)];
    [[UISlider appearance] setMaximumTrackTintColor:UIColorFromRGB(0x6f6f6f)];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view.superview setBackgroundColor:[UIColor whiteColor]];
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
    {
        [self prepareLandscapeWithDuration:0];
    }

    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
  
    self.womenCheckBox.selected = YES;
    isWoman = YES;

    //configure age slider
    self.ageSlider.minimumValue = 20.0;
    self.ageSlider.maximumValue = 75.0;
    
    minAge = self.ageSlider.lowerValue;
    maxAge = self.ageSlider.upperValue;
    [self updateAgeLabel];
    
    //configure distance slider
    self.distanceSlider.lowerHandleHidden = YES;
    self.distanceSlider.minimumValue = 4.0;
    self.distanceSlider.maximumValue = 99.0;
    self.distanceSlider.upperValue = 99.0;
    self.distanceSliderValueLabel.hidden = YES;
    self.distanceSliderValueView.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    float keyboardOffset;
    
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
        keyboardOffset = keyboardSize.width - 54;
    else
        keyboardOffset = keyboardSize.height - 64;

    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view setFrame:CGRectMake(0, -keyboardOffset, SelfViewWidth, SelfViewHeight)];
                         [self.view layoutIfNeeded];
                     }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    float keyboardOffset;
    UIInterfaceOrientation orientation = SharedApplication.statusBarOrientation;
    if (UIDeviceOrientationIsLandscape(orientation))
        keyboardOffset = 54;
    else
        keyboardOffset = 64;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view setFrame:CGRectMake(0, keyboardOffset, SelfViewWidth, SelfViewHeight)];
                         [self.view layoutIfNeeded];
                     }];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isEqual:self.ageSlider] || [touch.view isEqual:self.distanceSlider])
        return NO;
    return YES;
}

#pragma mark - Actions

-(void)goChat
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    ChatViewController *chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"chat"];
    [self.navigationController pushViewController:chatViewController animated:YES];
}


- (IBAction)changeAge:(NMRangeSlider *)sender
{
    minAge = sender.lowerValue;
    maxAge = sender.upperValue;
    [self updateAgeLabel];
}

- (IBAction)changeDistance:(NMRangeSlider *)sender
{
    self.distanceSliderValueLabel.hidden = NO;
    self.distanceSliderValueView.hidden = NO;
    distance = sender.upperValue;
    [self updateDistanceSliderLabel];
}

- (void)updateDistanceSliderLabel
{
    CGPoint upperCenter;
    upperCenter.x = (self.distanceSlider.upperCenter.x + self.distanceSlider.frame.origin.x);
    upperCenter.y = (self.distanceSlider.center.y - 30.0f);
    self.distanceSliderValueView.center = upperCenter;
    upperCenter.y = (self.distanceSlider.center.y - 33.0f);
    self.distanceSliderValueLabel.center = upperCenter;
    self.distanceSliderValueLabel.text = [NSString stringWithFormat:@"%d km", (int)self.distanceSlider.upperValue];
}

- (IBAction)womanCheckBoxClicked:(id)sender
{
    if (!(!isMen && isWoman)) {
        self.womenCheckBox.selected = !self.womenCheckBox.selected;
        isWoman = self.womenCheckBox.selected;
        [self updateAgeLabel];
    }
}

- (IBAction)menCheckBoxClicked:(id)sender
{
    if (!(isMen && !isWoman)) {
        self.menCheckBox.selected = !self.menCheckBox.selected;
        isMen = self.menCheckBox.selected;
        [self updateAgeLabel];
    }
}

- (void)updateAgeLabel
{
    NSString *text = [[NSString alloc] init];
    if (isWoman && isMen) text = @"Woman and man";
    if (isWoman && !isMen) text = @"Woman";
    if (!isWoman && isMen) text = @"Man";
    self.ageLabel.text = [NSString stringWithFormat:@"%@ ages %d to %d YO", text, minAge, maxAge];
}


- (IBAction)vibrateCkeckBoxClicked:(id)sender
{
    self.vibrateCheckBox.selected = !self.vibrateCheckBox.selected;
    vibrateOn = self.vibrateCheckBox.selected;
}

- (IBAction)soundCheckBoxClicked:(id)sender
{
    self.soundCheckBox.selected = !self.soundCheckBox.selected;
    soundOn = self.soundCheckBox.selected;
}

- (IBAction)goSearch:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)tapToView:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation))
       [self prepareLandscapeWithDuration:duration];
    else
        [self preparePortraitWithDuration:duration];
    
    [self updateDistanceSliderLabel];

}


- (void)prepareLandscapeWithDuration:(NSTimeInterval)duration
{
    CGRect screenRect = [UIScreen mainScreen].bounds;
       
    [UIView animateWithDuration:duration
                     animations:^{
                         self.selectCityLabel.alpha = 0.0;
                         distanceSliderWidth.constant = (screenRect.size.height / 2) - 40;
                         distanceSliderHeight.constant = 160.0;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:duration/2
                     animations:^{
                         self.secondSliderView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration
                                          animations:^{
                                              ageSliderTop.constant = 20.0;
                                              ageSliderLeft.constant = distanceSliderWidth.constant + 35.0;
                                              [self.view layoutIfNeeded];
                                          } completion:^(BOOL finished) {
                                              self.secondSliderView.alpha = 1.0;
                                          }];
                     }];

}

- (void)preparePortraitWithDuration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:duration
                     animations:^{
                         self.selectCityLabel.alpha = 1.0;
                         distanceSliderWidth.constant = 280.0;
                         distanceSliderHeight.constant = 143.0;
                         [self.view layoutIfNeeded];
                     }];
    
    [UIView animateWithDuration:duration/2
                     animations:^{
                        self.secondSliderView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration
                                          animations:^{
                                              ageSliderTop.constant = 183.0;
                                              ageSliderLeft.constant = 20.0;
                                              [self.view layoutIfNeeded];
                                          } completion:^(BOOL finished) {
                                              self.secondSliderView.alpha = 1.0;
                                          }];
                     }];

}


@end
