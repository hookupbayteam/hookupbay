//
//  CloudSockedWrapper.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper.h"
#import "CloudSockedWrapper+Connection.h"
#import "CloudSockedWrapper+Subscribe.h"

#define isEqualConnectionId [jsonDict[@"id"] isEqualToString:connectionID]
#define isEqualMessageId [jsonDict[@"mid"] isEqualToString:messageID]
#define is isEqualToString:

@interface CloudSockedWrapper ()
{
    NSTimer *reconectTimer;
}

@end

@implementation CloudSockedWrapper

-(void)setSocket:(CloudSocket *)socket
{
    _socket = socket;
    _socket.delegate = self;
}

#pragma mark - Cloud Socket Protocol methods

- (void)onConnect
{
    if (!isConnected) {
        [self establishConnection];
    }
}

- (void)onDisconnect
{
    isConnected = NO;
    isSessionActive = NO;
    
    if ([self.socket.inputStream streamStatus] == NSStreamStatusOpen) {
        [self.socket.inputStream close];
        [self.socket.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
    
    if ([self.socket.outputStream streamStatus] == NSStreamStatusOpen) {
        [self.socket.outputStream close];
        [self.socket.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
    
    reconectTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                     target:self
                                                   selector:@selector(tryReconnect)
                                                   userInfo:nil
                                                    repeats:NO];
}

- (void)tryReconnect
{
    if ([self.socket.outputStream streamStatus] != NSStreamStatusOpen ||
        [self.socket.inputStream streamStatus] != NSStreamStatusOpen) {
        NSLog(@"try reconnect");
        [self.socket initNetworkCommunication];
    } else {
        [reconectTimer invalidate];
        reconectTimer = nil;
    }
}

- (void)onJSONRecieved:(NSData *)json
{
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"%@", jsonDict);
    
    @try {
        NSString *result = jsonDict[@"internal"];
        
        switch (currentAction) {
            case OpenSocket:
                if ([result is @"socket:open"])
                    [self.delegate onConnectionEstablishing];
                break;
                
            case ConnectWithID:
                if ([result is @"connect:success" ] && isEqualConnectionId)
                    [self.delegate onConnectWithID];
                break;
                
                if ([result is @"socketApiResponse"] &&
                    isEqualConnectionId && isEqualMessageId && jsonDict[@"error"] == NULL) {
                    
                case AttachSessionToSocket:
                    myUserId = jsonDict[@"data"][@"id"];
                    userName = jsonDict[@"data"][@"name"];
                    [self.delegate onSessionAttached];
                    break;
                    
                case GetChannelAuthString:
                    authChannelString = jsonDict[@"data"][@"auth"];
                    channelID = jsonDict[@"data"][@"channel_id"];
                    channelData = jsonDict[@"data"][@"channel_data"];
                    [self subscribe];
                    break;
                    
                case SendChatMessage:
                    messageDateStamp = jsonDict[@"data"];
                    
                    break;
                }
                
            case GetSubscribeResult:
                if (isEqualConnectionId) {
                    channelID = jsonDict[@"channel"];
                    channelData = jsonDict[@"data"];

                    if ([jsonDict[@"channel"] isEqualToString:@"presence-lobby"] && [result is @"subscribe:success"])
                        [self.delegate onUserPresenceSubscribed];
                    
                    if ([jsonDict[@"channel"] rangeOfString:@"private-notification"].location != NSNotFound
                        && [result is @"subscribe:success"])
                        [self.delegate onPrivateMessageSubscribed];
                    
                    if ([jsonDict[@"channel"] rangeOfString:@"presence-conversation"].location != NSNotFound
                        && [result is @"subscribe:success"])
                        [self.delegate onMessageHistoryLoaded];
                }
                break;
                               
            case GetConversationHistory:
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exeption: %@", exception.description);
    }

}



@end
