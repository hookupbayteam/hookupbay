//
//  UIView+Border.h
//  TPW
//
//  Created by Denis Morozov on 16.09.13.
//  Copyright (c) 2013 By EZD Productions, USA. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	UIViewBorderNone = 0,
	UIViewBorderTop = 1 << 1,
	UIViewBorderRight = 1 << 2,
	UIViewBorderBottom = 1 << 3,
	UIViewBorderLeft = 1 << 4,
	UIViewBorderAll = (UIViewBorderTop | UIViewBorderRight | UIViewBorderBottom | UIViewBorderLeft),
	UIViewBorderWithoutTop = (UIViewBorderAll & (~UIViewBorderTop)),
	UIViewBorderWithoutRight = (UIViewBorderAll & (~UIViewBorderRight)),
	UIViewBorderWithoutBottom = (UIViewBorderAll & (~UIViewBorderBottom)),
	UIViewBorderWithoutLeft = (UIViewBorderAll & (~UIViewBorderLeft)),
}UIViewBorder;

@interface UIView (Border)

- (void)setBorder:(UIViewBorder)border;
- (void)setBorder:(UIViewBorder)border withColor:(UIColor *)color;
- (void)setBorder:(UIViewBorder)border withColor:(UIColor *)color withWidth:(CGFloat)width;

- (void)setBorderColor:(UIColor *)color withBorder:(UIViewBorder)border;
- (void)setBorderWidth:(CGFloat)width withBorder:(UIViewBorder)border;
@end
