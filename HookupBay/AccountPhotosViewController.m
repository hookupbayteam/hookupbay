//
//  AccountPhotosViewController.m
//  HookupBay
//
//  Created by Tatyana on 30.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "AccountPhotosViewController.h"

@interface AccountPhotosViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UILabel *photosCountLabel;

@end

@implementation AccountPhotosViewController

-(id)initWithPhoto:(UIImage *)photo
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"AccountPhotosViewController"];
	if (self)
	{
        if (photo) {
            self.photo = photo;
        }
	}
	return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.photoView.image = self.photo;
    [self.photoView sizeToFit];
    self.photosCountLabel.text = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)self.pageIndex+1 ,(unsigned long)self.totalPhotoCount];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
