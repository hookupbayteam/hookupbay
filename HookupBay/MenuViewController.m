//
//  MenuViewController.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "QuestionsViewController.h"
#import "ChatViewController.h"
#import "SettingsViewController.h"
#import "LikesViewController.h"
#import "Helper.h"

@interface MenuViewController ()

@property (nonatomic, strong) NSArray *menuItems;

@end

@implementation MenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _menuItems = @[@"Profile", @"Questions",  @"Chatapp", @"Settings", @"Likes"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*)segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menuItems count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [self.menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	   
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = UIColorFromRGB(0xffdbc2);
    [cell setSelectedBackgroundView:bgColorView];
    cell.textLabel.highlightedTextColor = UIColorFromRGB(0x484445);
    
    //setup icons for each row
    NSString *imageName = [[NSString alloc] init];
    switch (indexPath.row) {
        case 0: //avatar
            imageName = @"profile_item";
           // image = [self appDelegate].currentUser.avatar;
            break;
        case 1:
            imageName = @"questions_item";
            break;
        case 2:
            imageName = @"chat_item";
            break;
        case 3:
            imageName = @"settings_item";
            break;
        case 4:
            imageName = @"likes_item";
            break;
    }
    cell.imageView.image = [UIImage imageNamed:imageName];;

    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(35, cell.frame.size.height, 260, 0.3)];
    NSInteger activeCell = 0;
    
    if ([self.source isKindOfClass:[ProfileViewController class]])
        activeCell = 0;
    if ([self.source isKindOfClass:[QuestionsViewController class]])
        activeCell = 1;
    if ([self.source isKindOfClass:[ChatViewController class]])
        activeCell = 2;
    if ([self.source isKindOfClass:[SettingsViewController class]])
        activeCell = 3;
    if ([self.source isKindOfClass:[LikesViewController class]])
        activeCell = 4;
    
    if (indexPath.row == activeCell) {
        line.backgroundColor = UIColorFromRGB(0x484445);
    } else {
        line.backgroundColor = UIColorFromRGB(0xeeeeee);
    }
    [cell addSubview:line];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 85;
    else
        return 80;
}


@end
