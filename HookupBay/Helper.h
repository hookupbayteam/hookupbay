//
//  Helper.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 19.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ApplicationDelegate ((MyAppDelegate *)[[UIApplication sharedApplication] delegate])
#define UserDefaults        [NSUserDefaults standardUserDefaults]
#define SharedApplication   [UIApplication sharedApplication]
#define SelfViewWidth       self.view.frame.size.width
#define SelfViewHeight      self.view.frame.size.height

#define IS_IPHONE_5 ( [ [ UIScreen mainScreen ] bounds ].size.height == 568 )