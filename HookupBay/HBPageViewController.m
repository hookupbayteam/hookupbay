//
//  HBPageViewController.m
//  HookupBay
//
//  Created by Tatyana on 31.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "HBPageViewController.h"

@interface HBPageViewController ()

@end

@implementation HBPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidLayoutSubviews
{
    // Adjust the subviews' positions
    for (UIViewController *viewController in self.childViewControllers)
    {
        viewController.view.frame = self.view.frame;
    }
}

@end
