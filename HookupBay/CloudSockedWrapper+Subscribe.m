//
//  CloudSockedWrapper+Subscribe.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CloudSockedWrapper+Subscribe.h"

@implementation CloudSockedWrapper (Subscribe)

- (void)subscribeToUserPresence
{
   [self getAuthStringForChannel:@"presence-lobby"];
}

- (void)subscribeToPrivateMessage
{
    [self getAuthStringForChannel:[NSString stringWithFormat:@"private-notification-%@", myUserId]];
}

-(void)subscribeToHistoryWithUser:(NSNumber *)user
{
   [self getAuthStringForChannel:[NSString stringWithFormat:@"presence-conversation-%@-%@", myUserId, user]];
}

- (void)getAuthStringForChannel:(NSString *)channelName
{
    currentAction = GetChannelAuthString;
    NSDictionary *messageDict = @{@"id": connectionID,
                                  @"mid" : messageID,
                                  @"internal" : @"socketApi",
                                  @"data" : @{@"controller": @"auth",
                                              @"action" : @"channel",
                                              @"params" : @{@"connection_id" : connectionID,
                                                            @"channel_id" : channelName}
                                              }
                                  };
    [self.socket sendJSON:messageDict];
}

- (void)subscribe
{
    currentAction = GetSubscribeResult;
    NSDictionary *messageDict = @{@"id": connectionID,
                                  @"internal" : @"subscribe",
                                  @"data" : @{@"auth": authChannelString,
                                              @"channel_id" : channelID,
                                              @"channel_data" : channelData}
                                  };
    [self.socket sendJSON:messageDict];
}


@end
