//
//  ChatViewController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatUser.h"

@interface ChatViewController : UIViewController

@property (strong, nonatomic) ChatUser *user;
@property (strong, nonatomic) NSMutableArray *messages;

@end
