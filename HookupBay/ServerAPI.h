//
//  ServerAPI.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 22.08.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudSockedWrapper.h"

#define LOGIN_HOST @"http://54.83.43.55/mobileApi/login"
#define temp_login @"francia@gmail.com"
#define temp_pass @"123456"

typedef void (^SessionIdCompletionHandler) (NSString *sessionID);

@interface ServerAPI : NSObject

@property (nonatomic, strong) CloudSockedWrapper *socketWrapper;

@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userPassword;

+(void)getSessionIdWithCompletionHandler:(SessionIdCompletionHandler)completionHandler;

@end
