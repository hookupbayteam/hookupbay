//
//  AnimationTabbarViewController.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 31.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimationTabbarViewController : UIViewController <UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate>

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) BOOL reverse;
@property (nonatomic, assign) BOOL isPresenting;

@end
