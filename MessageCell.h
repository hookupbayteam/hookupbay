//
//  MessageCell.h
//  HookupBay
//
//  Created by Tatyana Mudryak on 25.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface MessageCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *messageView;
@property (strong, nonatomic) IBOutlet UIView *selfMessageView;

@property (strong, nonatomic) IBOutlet UITextView *selfMessageTextView;
@property (strong, nonatomic) IBOutlet UIImageView *selfAvatarImage;
@property (strong, nonatomic) IBOutlet UILabel *selfMessageTimeLabel;

@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UILabel *messageTimeLabel;


@property (nonatomic, strong) NSString *messageText;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) NSString *user;


-(void)configureCell;
@end
