//
//  ProfilePictureCell.m
//  HookupBay
//
//  Created by Tatyana Mudryak on 24.07.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "ProfilePictureCell.h"
#import "UIView+Border.h"
#import "Helper.h"

@implementation ProfilePictureCell

-(void)setSelected:(BOOL)selected
{
    if (selected) {
        [self setBorder:UIViewBorderAll withColor:UIColorFromRGB(0xff9f70) withWidth:2.5];
        self.settingsButton.hidden = NO;
    }
    else {
        [self setBorder:UIViewBorderNone];
         self.settingsButton.hidden = YES;
    }
}

@end
